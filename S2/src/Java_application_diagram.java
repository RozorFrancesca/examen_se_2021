public class Java_application_diagram {

    //association
    class Y{

    }

    //B
    class Employee {
       private String name;
       private Address address;
       private Job job;
    }

    //include
    class Z {

    }

    //E
    //aggregation
    class Address{
        String city,state,country;
    }

    //C
    //composition
    class Job {
        private String role;
        private long salary;
        private int id;

    }

    class I {

    }


}
