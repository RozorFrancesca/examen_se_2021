import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GUI_application {
    private JFrame     frame;
    private JPanel     panel;
    private JButton    btn;
    private JLabel     lb;
    private JTextField field1;
    private JTextField field2;

    // Constructor
    public GUI_application() {
        // Create the textfield to read input
        field1 = new JTextField( 30 );
        field1.setBounds( 10, 10, 240, 20 );

        field2 = new JTextField( 30 );
        field2.setBounds( 10, 40, 240, 20 );
        // Sets the specified boolean to indicate whether or not
        // this textfield should be editable.
        field2.setEditable(false);

        // Create the button
        btn = new JButton( "Ok" );
        btn.addActionListener( new btnSubmitAction( this ) );
        btn.setBounds( 260, 70, 100, 20 );

        // Create the label to display the result
        lb = new JLabel( "Enter input and then press Ok" );
        lb.setBounds( 10, 90, 320, 20 );

        // Create the panel to hold the button, label, and textfields
        panel = new JPanel( null );
        panel.add( btn );
        panel.add( lb );
        panel.add( field1 );
        panel.add( field2 );
        panel.setPreferredSize( new Dimension(370, 150) );

        // Create the frame which is a window
        frame = new JFrame( "GUI" );
        frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        frame.getContentPane().add( panel );
        frame.pack();
        frame.setVisible( true );
    }

    public String getTextFieldInput() {
        return field1.getText();
    }

    public void setTextField2(String s) {
        field2.setText(s);
    }

    public static void main( String[] args ) {
        new GUI_application();
    }
}

class btnSubmitAction implements ActionListener {

    private GUI_application g;

    public btnSubmitAction( GUI_application g ) {
        this.g = g;
    }

    @Override
    public void actionPerformed( ActionEvent e ) {
        String s = g.getTextFieldInput();
        g.setTextField2(s);
    }
}
